const jwt = require("jsonwebtoken");

module.exports = async (req, res, next) => {
  try {
    const token = req.header("x-token");
    console.log("token", token);
    if (!token) {
      return res.status(400).send("Token not found");
    }
    const docoded = jwt.verify(token, "jwtSecret");
    req.user = docoded.user;
    next();
  } catch (error) {
    return res.status(500).send("Internal server error");
  }
};
