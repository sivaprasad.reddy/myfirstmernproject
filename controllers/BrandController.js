const express = require("express");
const BrandName = require("../models/BrandModel");
const router = express.Router();

router.post("/addbrands", async (req, res) => {
  const { brandname } = req.body;
  try {
    const newData = new BrandName({ brandname });
    await newData.save();
    return res.json(await BrandName.find());
  } catch (error) {
    console.log("errror", error.message);
  }
});

router.get("/getallbrands", async (req, res) => {
  try {
    const getAllData = await BrandName.find();
    return res.json(getAllData);
  } catch (error) {
    console.log(error);
  }
});
router.get("/getallbrands/:id", async (req, res) => {
  try {
    const brnadGetById = await BrandName.findById(req.params.id);
    return res.json(brnadGetById);
  } catch (error) {
    console.log(error);
  }
});

router.delete("/deletebrand/:id", async (req, res) => {
  try {
    await BrandName.findByIdAndDelete(req.params.id);
    return res.json(BrandName.find());
  } catch (error) {}
});

module.exports = router;