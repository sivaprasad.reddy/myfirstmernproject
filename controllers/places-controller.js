const HttpError = require("../models/http-error");
const uuid =require("uuid").v4
const DUMMY_PLACES = [
  {
    id: "p1",
    title: "Empire State Bulding",
    description: "One of the most famous Sky scrapers in the world.!",
    location: {
      lat: 40.7484405,
      lng: 3.9856644,
    },
    address:
      "Iconic, art deco office tower from 1931 with exhibits & observatories on the 86th & 102nd floors",
    creator: "u1",
  },

];
const getPlaceByID = (req, res, next) => {
  const placeId = req.params.pid;
  const place = DUMMY_PLACES.find((p) => p.id === placeId);
  if (!place) {
    throw new HttpError("Could not found a place for the provided id!", 404);
    // const error =new Error("Could not found a place for the provided id.")
    // error.code =404
    // throw error;
    //    return res.status(404).json({message:"could not found a place for the provided id."})
  }
  res.send({ place });
};

const getPlaceByUserID = (req, res, next) => {
  const userId = req.params.uid;
  const place = DUMMY_PLACES.find((p) => p.creator === userId);
  if (!place) {
    return next(
      new HttpError("Could not found a place for the provided User id!", 404)
    );

    //     const error =new Error("Could not found a place for the provided User id.")
    //     error.code =404
    //    return next(error)
    // return res.status(404).json({message:"could not found a place for the provided user id."})
  }
  res.send({ place });
};

const createPlace = (req, res, next) => {
    console.log("req.body",req.body)
  const { title, description, coordinates, address, creator } = req.body;
  const createdPlace = {
    id:uuid(),
    title,
    description,
    location: coordinates,
    address,
    creator,
  };
  DUMMY_PLACES.push(createdPlace);
  res.status(201).json({ palace: createdPlace });
};

const upatePlace=((req,res,next)=>{})
const deletePlace=((req,res,next)=>{})

exports.getPlaceByUserID = getPlaceByUserID;
exports.getPlaceByID = getPlaceByID;
exports.createPlace = createPlace;
exports.upatePlace = upatePlace;
exports.deletePlace = deletePlace;


