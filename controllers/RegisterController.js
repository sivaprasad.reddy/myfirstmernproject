const Registeruser = require("../models/RegisterModel");
const middlware = require("../middleware");
const express = require("express")
const jwt = require("jsonwebtoken");
const router = express.Router();

// Register Api
router.post("/registeruser", async (req, res) => {
    const { username, email, password, confirmpassword } = req.body;
    try {
      const registerUser = new Registeruser({
        username,
        email,
        password,
        confirmpassword,
      });
      const exist = await Registeruser.findOne({ email });
      if (exist) {
        return res.status(400).json("email already exist");
      }
      console.log("password", password);
      console.log("confirmpassword", confirmpassword);
      if (password !== confirmpassword) {
        return res.status(400).json("password confirmpassword are not same");
      }
      await registerUser.save();
      return res.status(200).send("user register successfully");
    } catch (error) {
      console.log("error", error);
      return res.status(500).json("Internal server error");
    }
  });
  // Login Api
  router.post("/login", async (req, res) => {
    const { email, password } = req.body;
    console.log( "req.body", req.body)
    try {
      const existUser = await Registeruser.findOne({ email });
      if (!existUser) {
        return res.status(400).send("email id is incorrect");
      }
      if (existUser.password !== password) {
        return res.status(400).send("password is incorect");
      }
      const payload = {
        user: {
          id: existUser.id,
        },
      };
      jwt.sign(payload, "jwtSecret", { expiresIn: 36000000 }, (err, token) => {
        if (err) throw err;
        return res.status(200).send({ token });
      });
    } catch (error) {
      console.log("error===>",error)
      return res.status(500).send("Internal server error");
    }
  });
  // User list with protected route
  router.get("/myprofile", middlware, async (req, res) => {
    console.log("exist", req.user);
    try {
      const exist = await Registeruser.findById(req.user.id);
      if (!exist) {
        return res.status(400).send("error");
      }
      res.send(exist);
    } catch (error) {}
  });
  module.exports = router;