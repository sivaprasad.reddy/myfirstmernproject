const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const connectDB = require("./db");
const RegisterController = require("./controllers/RegisterController");
const BrandController = require("./controllers/BrandController");
const PlacesRoutes = require("./routes/places-routes");
const HttpError = require("./models/http-error");

const app = express();

//  app.use(express.json());
app.use(bodyParser.json())
app.use(cors({ origin: "*" }));

// RegisterController
// app.use(RegisterController);
// //BrandController
// app.use(BrandController);
//Places
app.use("/api/places", PlacesRoutes);

//if invalid route
app.use((req,res,next)=>{
  const error = new HttpError("Could not found this route",404)
  throw error
})
// Error handling
app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res
    .status(error.code || 500)
    .json({ message: error.message || "An unknow error occurred!" });
});
app.listen(5000, "localhost", () => {
  console.log("listening on port 8000.....");
});

connectDB();
// magirisivareddy
// hMC5ZXSct0wBMpQD
