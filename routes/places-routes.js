const express = require("express");
const placesControllers = require("../controllers/places-controller");

const router = express.Router();

router.get("/", (req, res, next) => {
  console.log("Get works");
});
router.get("/:pid", placesControllers.getPlaceByID);
router.get("/user/:uid", placesControllers.getPlaceByUserID);
router.post("/", placesControllers.createPlace);
router.patch("/:pid", placesControllers.upatePlace);
router.delete("/:pid", placesControllers.deletePlace);

module.exports = router;
